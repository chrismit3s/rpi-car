

class Servo():
    def __init__(self, pca, channel, offset=0, scale=1, name="Servo"):
        self.pwm = pca.channels[channel]
        self.offset = offset
        self.scale = scale

        self.name = name
        self._position = None

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, value):
        if not (-1 <= value <= 1):
            raise ValueError("Servo position needs to be in [-1, 1], got {:f}".format(value))
        self._position = value
        self.pwm.duty_cycle = self._position_to_duty_cycle(value * self.scale + self.offset)

    def _position_to_duty_cycle(self, position):
        """ angle from [-1, 1]; returns the duty cycle """
        return int(0xFFFF * (8.25 + 5.25 * position) / 100)

    def _find_border_once(self, start, step, is_skipping_prompt):
        x = start
        print("- stepping at", step)
        while True:
            self.pwm.duty_cycle = int(self._position_to_duty_cycle(x))
            if input(is_skipping_prompt).strip().lower() == "n":
                return x
            x += step

    def _find_border(self, start, step, n, is_skipping_prompt):
        best = start
        prev_step = 0
        curr_step = step
        for _ in range(n):
            # we overstep once to get too big
            best = self._find_border_once(best - prev_step, curr_step, is_skipping_prompt)
            prev_step = curr_step
            curr_step *= abs(step)  # abs to keep the sign
        return best

    def calibrate(self, step=0.2, n=4, is_skipping_prompt="skipping? (Y/n) "):
        left = self._find_border(-1 - step, step, n, is_skipping_prompt)  # start too low and get bigger
        right = self._find_border(1 + step, -step, n, is_skipping_prompt)  # start too big and get smaller

        self.offset = (left + right) / 2
        self.scale = (right - left) / 2

        self.position = 0
