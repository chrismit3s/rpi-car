from src import Car
from time import sleep


def keyboard_interrupt():
    raise KeyboardInterrupt


def main(car):
    # calibrate steering sensor
    if input("calibrate? (y/N) ").strip().lower() == "y":
        car.calibrate(is_skipping_prompt="> skipping= (Y/n) ")
    print("- steering calibration: offset={:.5f}, scale={:.5f}".format(car.steering.offset, car.steering.scale))
    print("- camera yaw calibration: offset={:.5f}, scale={:.5f}".format(car.camera_yaw.offset, car.camera_yaw.scale))
    print("- camera pitch calibration: offset={:.5f}, scale={:.5f}".format(car.camera_pitch.offset, car.camera_pitch.scale))

    # command handlers
    handle_cmd = {
            "left": lambda speed: car.set_speed_left(speed),
            "right": lambda speed: car.set_speed_right(speed),
            "both": lambda *speeds: car.set_speed(*speeds),
            "stop": lambda *speeds: car.set_speed(0),

            "turn": lambda position: car.turn_to(position),

            "default": lambda *speeds: car.default(),
            "!": lambda *speeds: car.default(),

            "yaw": lambda yaw: car.look_at(yaw=yaw),
            "pitch": lambda pitch: car.look_at(pitch=pitch),
            "look": lambda yaw, pitch: car.look_at(yaw=yaw, pitch=pitch),

            "quit": lambda: keyboard_interrupt(),
            "exit": lambda: keyboard_interrupt()}
    handle_cmd["help"] = lambda: print("- available commands:", ", ".join(handle_cmd.keys()))

    while True:
        try:
            cmd, *args = input("> ").strip().split(" ")
            float_args = [float(arg) for arg in args]
            if cmd in handle_cmd.keys():
                handle_cmd[cmd](*float_args)
            else:
                print("! invalid command")

        except ValueError:
            print("! invalid argument")


if __name__ == "__main__":
    car = Car("config.json")
    try:
        main(car)
    except KeyboardInterrupt:
        pass
    finally:
        car.default()
