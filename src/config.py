import board
import json


def to_pin(s):
    if isinstance(s, str) and hasattr(board, s):
        return getattr(board, s)
    return s


def all_values_to_pins(d):
    for (k, v) in d.items():
        d[k] = to_pin(v)
    return d


def parse_config(filename):
    with open(filename, "r") as fp:
        return json.load(fp, object_hook=all_values_to_pins)
