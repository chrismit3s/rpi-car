from digitalio import DigitalInOut, Direction


class Motor():
    def __init__(self, pca, channel, forward_pin, backward_pin, name="Motor"):
        self.pwm = pca.channels[channel]

        self.forward = DigitalInOut(forward_pin)
        self.forward.direction = Direction.OUTPUT
        self.backward = DigitalInOut(backward_pin)
        self.backward.direction = Direction.OUTPUT

        self.name = name
        self._speed = None

    @property
    def speed(self):
        return self._speed

    @speed.setter
    def speed(self, value):
        if not (-1 <= value <= 1):
            raise ValueError("Motor speed needs to be in [-1, 1], got {:f}".format(value))
        self._speed = value
        self.pwm.duty_cycle = int(0xFFFF * abs(value))
        self.forward.value = (value > 0)
        self.backward.value = (value < 0)
