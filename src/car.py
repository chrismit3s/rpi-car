from adafruit_pca9685 import PCA9685
from busio import I2C
from src import Servo, Motor, parse_config
import board


class Car():
    def __init__(self, config):
        if isinstance(config, str):
            config = parse_config(config)

        self.pca = PCA9685(I2C(board.SCL, board.SDA), address=config["i2c_address"])
        self.pca.frequency = 50

        self.left_motor = Motor(self.pca, name="left_motor", **config["left_motor"])
        self.right_motor = Motor(self.pca, name="right_motor", **config["right_motor"])

        self.steering = Servo(self.pca, name="steering", **config["steering"])
        self.camera_yaw = Servo(self.pca, name="camera_yaw", **config["camera_yaw"])
        self.camera_pitch = Servo(self.pca, name="camera_pitch", **config["camera_pitch"])

    @property
    def motors(self):
        return (self.left_motor, self.right_motor)

    @property
    def servos(self):
        return (self.steering, self.camera_yaw, self.camera_pitch)

    def turn_to(self, position):
        print("turn")
        self.steering.position = position

    def set_speed(self, speed, speed_=None):
        print("speed")
        self.left_motor.speed = speed
        self.right_motor.speed = speed_ or speed

    def set_speed_left(self, speed):
        self.left_motor.speed = speed

    def set_speed_right(self, speed):
        self.right_motor.speed = speed

    def look_at(self, yaw=None, pitch=None):
        if yaw is not None:
            self.camera_yaw.position = yaw
        if pitch is not None:
            self.camera_pitch.position = pitch

    def calibrate(self, **kwargs):
        self.default()
        for servo in self.servos:
            servo.calibrate(**kwargs)

    def default(self):
        for motor in self.motors:
            motor.speed = 0
        for servo in self.servos:
            servo.position = 0
